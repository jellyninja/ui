import React from 'react'
import { View, Text } from 'react-native'
import styles from './styles'

export default () => (
  <View style={styles.container}>
    <Text>Registration page</Text>
  </View>
)
