export const useTranslation = () => ({
  t: (s: string) => s,
  i18n: { changeLanguage: () => Promise.resolve() },
})
