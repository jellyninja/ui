import React from 'react'
import { Platform, SafeAreaView } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import { Registration } from '@jelly-ninja/trial'

export default () => {
  return (
    <SafeAreaView
      style={
        Platform.OS === 'android' ? { paddingTop: 30, flex: 1 } : { flex: 1 }
      }
    >
      <StatusBar />
      <Registration />
    </SafeAreaView>
  )
}
