module.exports = {
  plugins: [
    [
      'i18next-extract',
      {
        locales: ['zh', 'en'],
        keyAsDefaultValue: ['zh'],
      },
    ],
  ],
  presets: ['module:metro-react-native-babel-preset'],
}
